import {Lambda} from '@aws-sdk/client-lambda'
import {fromIni} from '@aws-sdk/credential-provider-ini'

const FUNCTION_NAME = 'Greeter'

let lambdaClient = new Lambda({
    credentials: fromIni(),
})

let invocation = lambdaClient.invoke({
    FunctionName: FUNCTION_NAME,
    Payload: encodePayload({
        firstName: "Marco",
        lastName: "Sieben",
        age: 18
    })
})

invocation.then(result => {
    console.log(decodePayload(result.Payload))
}, reason => {
    console.log(reason)
})

function encodePayload(payload: any) {
    return new TextEncoder().encode(JSON.stringify(payload))
}

function decodePayload(payload: Uint8Array | undefined) {
    if (payload === undefined) {
        return undefined
    }
    return JSON.parse(new TextDecoder().decode(payload))
}