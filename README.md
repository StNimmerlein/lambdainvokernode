# Invoke AWS Lambda via SDK

This demo is created to be used together with the Lambda function created by https://gitlab.com/StNimmerlein/lambdajava or https://gitlab.com/StNimmerlein/lambdanode.

## Prerequisites

You need to have the aws cli installed and configured with your credentials and region (a `config` and `credentials` in your `~/.aws/` directory).

## Run

In `src/invokeLambda.ts`, put your function name, modify the payload to your liking and run the program (`yarn run` or `yarn build && node dist/invokeLambda.js`).

If you have multiple AWS profiles configured, you can specify which one to use by passing it's name to fromIni.

